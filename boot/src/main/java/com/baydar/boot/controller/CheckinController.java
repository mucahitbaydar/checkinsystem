package com.baydar.boot.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baydar.boot.Checkin;
import com.baydar.boot.exception.ResourceNotFoundException;
import com.baydar.boot.repository.CheckinRepository;

@RestController
@RequestMapping("/api")
public class CheckinController {

    @Autowired
    CheckinRepository checkinRepository;
    
 // Get All Checkins
    @GetMapping("/checkins")
    public List<Checkin> getAllCheckins() {
        return checkinRepository.findAll();
    }

 // Create a new Checkin
    @PostMapping("/checkins")
    public Checkin createCheckin(@Valid @RequestBody Checkin checkin) {
        return checkinRepository.save(checkin);
    }
    
 // Get a Single Checkin
    @GetMapping("/checkins/{id}")
    public Checkin getCheckinById(@PathVariable(value = "id") Long checkinId) {
        return checkinRepository.findById(checkinId)
                .orElseThrow(() -> new ResourceNotFoundException("Checkin", "id", checkinId));
    }
    
    
 // Update a Checkin
    @PutMapping("/checkins/{id}")
    public Checkin updateCheckin(@PathVariable(value = "id") Long checkinId,
                                            @Valid @RequestBody Checkin checkinDetails) {

    	Checkin checkin = checkinRepository.findById(checkinId)
                .orElseThrow(() -> new ResourceNotFoundException("Checkin", "id", checkinId));

    	checkin.setPlace_id(checkinDetails.getPlace_id());
    	checkin.setUser_id(checkinDetails.getUser_id());

    	Checkin updatedCheckin = checkinRepository.save(checkin);
        return updatedCheckin;
    }
    
 // Delete a Checkin
    @DeleteMapping("/checkins/{id}")
    public ResponseEntity<?> deleteCheckin(@PathVariable(value = "id") Long checkinId) {
    	Checkin checkin = checkinRepository.findById(checkinId)
                .orElseThrow(() -> new ResourceNotFoundException("Checkin", "id", checkinId));

    	checkinRepository.delete(checkin);

        return ResponseEntity.ok().build();
    }

    // Delete all Checkins
    @DeleteMapping("/checkins/")
    public ResponseEntity<?> deleteAllCheckins() {
    	List<Checkin> checkins = getAllCheckins();
    	for(int i = 0;i<checkins.size();i++) {
    		checkinRepository.delete(checkins.get(i));
    	}
        return ResponseEntity.ok().build();
    }
    


}
