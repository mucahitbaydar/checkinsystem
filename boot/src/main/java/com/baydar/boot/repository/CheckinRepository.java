package com.baydar.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.baydar.boot.Checkin;

@Repository
public interface CheckinRepository extends JpaRepository<Checkin, Long> {

}
