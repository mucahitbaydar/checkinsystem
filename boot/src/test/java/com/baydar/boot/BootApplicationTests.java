package com.baydar.boot;

import java.net.URI;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootApplicationTests {

    public static final String REST_SERVICE_URI = "http://localhost:8080/api";
    
    /* GET */
    @SuppressWarnings("unchecked")
    private static void listAllCheckins(){
        System.out.println("Testing listAllCheckins API-----------");
         
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> appMap = restTemplate.getForObject(REST_SERVICE_URI+"/checkins/", List.class);
         
        if(appMap!=null){
            for(LinkedHashMap<String, Object> map : appMap){
                System.out.println("Checkin : id="+map.get("id")+", User_id="+map.get("user_id")+", Place_id="+map.get("place_id")+", CreatedAt="+map.get("createdAt"));
            }
        }else{
            System.out.println("No checkins exist----------");
        }
    }
     
    /* GET */
    private static void getCheckin(){
        System.out.println("Testing getCheckin API----------");
        RestTemplate restTemplate = new RestTemplate();
        Checkin checkin = restTemplate.getForObject(REST_SERVICE_URI+"/checkins/2", Checkin.class);
        System.out.println(checkin);
    }
     
    /* POST */
    private static void createCheckin() throws ParseException {
        System.out.println("Testing create Checkin API----------");
        RestTemplate restTemplate = new RestTemplate();
        Checkin checkin = new Checkin();
        checkin.setUser_id(111);
        checkin.setPlace_id("ABCDE");
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/checkins/", checkin, Checkin.class);
        System.out.println(checkin);
    }
 
    /* PUT */
    private static void updateCheckin() throws ParseException {
        System.out.println("Testing update Checkin API----------");
        RestTemplate restTemplate = new RestTemplate();
        Checkin checkin  = new Checkin();
        checkin.setId(1l);
        checkin.setUser_id(99);
        checkin.setPlace_id("BCDEF");
        restTemplate.put(REST_SERVICE_URI+"/checkins/9", checkin);
        System.out.println(checkin);
    }
 
    /* DELETE */
    private static void deleteCheckin() {
        System.out.println("Testing delete Checkin API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/checkins/1");
    }
 
 
    /* DELETE */
    private static void deleteAllCheckins() {
        System.out.println("Testing delete all Checkins API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/checkins/");
    }
 
    public static void main(String args[]) throws ParseException{
        listAllCheckins();
        getCheckin();
        createCheckin();
        listAllCheckins();
        updateCheckin();
        listAllCheckins();
        deleteCheckin();
        listAllCheckins();
        deleteAllCheckins();
        listAllCheckins();
    }
	
	@Test
	public void contextLoads() {
	}

}
