## Checkin System
Checkin system where checkins can be created, deleted or updated.

## Motivation
This is an exercise project to go into learning spring boot and its features, gradle, jpa and hibernate. 

## Installation
You can download the project using git
```
git clone https://mucahitbaydar@bitbucket.org/mucahitbaydar/checkinsystem.git
```

First you need to create a database called 'checkins' in mysql
Change username and password in the application.properties file in resources

Then you can build and create jar file using gradle

```
gradle build
```

##Usage
Once you started application you can use Rest commands 
(Assuming you didn't change any configuration)

To see every checkins in system send Get request with
```
http://localhost:8080/api/checkins/
```

To delete checkin with id send Delete request with
```
http://localhost:8080/api/checkins/2
```